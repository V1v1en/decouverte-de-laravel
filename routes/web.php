<?php

// Non obligatoire ROUTE =
use Illuminate\Support\Facades\Route;

Route::name('home') -> get ('/', function (){
    return view('pages/home');
});

// Ajout du nom de la route aven ou après la fonction

Route::get('/about-us', function (){
    return view('pages/about');
})->name('about');
