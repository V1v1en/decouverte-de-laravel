

@extends('base')

@section('title', 'About us | ' . env('APP_NAME'))

<img src="{{asset('images/about.jpg')}}" alt="image grue construction" width="300px" class="rounded-full">

@section('content')

    <p>Built with <span class="text-pink-500">&hearts;</span> by Vivien Bergeron.</p>
    <p class="mt-5 font-bold text-gray-500 hover:text-gray-800"><a href="{{ route('home')}}">Revenir à la page d'accueil</a></p>

@endsection
