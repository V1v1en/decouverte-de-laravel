

@extends('base')

@section('title', env('APP_NAME'))

<img src="{{ asset('images/construct.jpg')}}" alt="image point interogation" width="300px" class="rounded-full">



@section('content')

    <h1 class="text-gray-600 text-3xl sm:text-5xl font-bold mt-2 mb-10">Site en construction</h1>
    <p class="text-gray-800 text-lg mt-5">Il est actuellement {{date('h:i A')}}</p>

@endsection
