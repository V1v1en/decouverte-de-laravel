<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Framework tailwind -->

    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */
    </style>


</head>

<body class="py-12 flex flex-col justify-between min-h-screen items-center">

    <main role="main" class="flex flex-col justify-center items-center">
    @yield('content')



    </main>

    <footer>
        <p class="text-gray-400">
            &copy; Copyright {{date('Y')}}

            @if(! Route::is('about'))  <!--si la route n'est pas about alors le lien ne s'affiche pas  -->

            &middot; <a href="{{route('about')}}" target="_self" class='text-indigo-500 hover:text-indigo-600 underline'>About us</a>
            @endif
        </p>
    </footer>


</body>

</html>
